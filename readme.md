Deployed app in : https://angular-zetta.herokuapp.com/

# Angular - Challenge, Gama

Project ini di generate menggunakan [Angular CLI](https://github.com/angular/angular-cli) versi 11.2.12.

## Stack yang digunakan

| Front End  | Library Tambahan | Deploy |
| :--------: | :--------------: | :----: |
| Angular Js |    Typescript    | Heroku |

<img src ="app-screen.JPG" width=570>

## Menjalankan Proyek ini di Local

1. Git Clone repositori dan masuk ke dalam folder proyek

   ```
   git clone https://gitlab.com/zetta_challenge/challenge_angular_gama.git

   cd Challenge_Angular_Gama

   ```

2. Install dependencies

   ```
   npm install

   ```

3. Buka terminal dan jalankan proyek dengan perintah

   ```
   `ng serve`

   ```

4. Menggunakan browser, akses proyek di `http://localhost:4200/`

## Deploy Aplikasi dapat dikunjungi di https://angular-zetta.herokuapp.com/
