import { Injectable } from '@angular/core';

// import interface
import { CertifierList } from './interface/certifier';
import { CERTIFIERLIST } from './title-list';

@Injectable({
  providedIn: 'root',
})
export class CertifierService {
  constructor() {}

  getCertifierList(): CertifierList[] {
    return CERTIFIERLIST;
  }
}
