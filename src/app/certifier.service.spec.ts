import { TestBed } from '@angular/core/testing';

import { CertifierService } from './certifier.service';

describe('CertifierService', () => {
  let service: CertifierService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CertifierService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
