export interface CertifierList {
  certifier: Certifier;
  is_published: boolean;
  long_name: string;
  short_name: string;
  rncp_level: string;
  rncp_level_europe: string;
  _id: string;
}

export interface Certifier {
  _id: string;
  short_name: string;
}
