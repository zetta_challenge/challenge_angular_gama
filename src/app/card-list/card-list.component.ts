import { Component, OnInit } from '@angular/core';

// import data
import { CERTIFIERLIST } from '../title-list';

// import service
import { CertifierList } from '../interface/certifier';
import { CertifierService } from '../certifier.service';

@Component({
  selector: 'app-card-list',
  templateUrl: './card-list.component.html',
  styleUrls: ['./card-list.component.scss'],
})
export class CardListComponent implements OnInit {
  certifierList: CertifierList[] = [];

  searchValue: any;

  filteredList?: CertifierList[];

  constructor(private certifierService: CertifierService) {}

  ngOnInit(): void {
    this.getCertifier();
  }

  getCertifier(): void {
    this.certifierList = this.certifierService.getCertifierList();
  }

  Search() {
    if (this.searchValue == '') {
      this.ngOnInit();
    } else {
      this.certifierList = this.certifierList.filter((res) => {
        return res.short_name
          .toLocaleLowerCase()
          .match(this.searchValue.toLocaleLowerCase());
      });
    }
  }
}
